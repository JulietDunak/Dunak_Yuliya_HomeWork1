package Task_1;

import java.util.ArrayList;
import java.util.List;

public class Task_1 {
    public static void main(String[] args) {
        System.out.println("********Task_1_1**********");
              // Створити List, заповнюємо іменами (10 імен), 3 ім'я змінюємо на 8 і навпаки.
        List<String> massName = new ArrayList<String>();
        massName.add("David");
        massName.add("Andrew");
        massName.add("Logan");
        massName.add("Jacob");
        massName.add("David");
        massName.add("Anthony");
        massName.add("Samuel");
        massName.add("James");
        massName.add("David");
        massName.add("Lucas");
        String name3 = massName.get(2);
        massName.set(2, massName.get(7));
        massName.set(7, name3);
        System.out.print(massName.get(2) + "\n" + massName.get(7) + "\n");
         System.out.println("********Task_1_2**********");
        // Написати програму для роботи з списком. У першій половині списку замінити всі входження деякого елементу Е1 на будь-який інший елемент Е2;
        for (int i = 0; i < massName.size() / 2; i++) {
            if (massName.get(i) == "David") {
                massName.set((i), "Oleg");
            }
        }
        for (int i = 0; i < massName.size(); i++) {
            System.out.print(massName.get(i) + " ");
        }
    }
}