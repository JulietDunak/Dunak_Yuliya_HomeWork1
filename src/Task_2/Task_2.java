package Task_2;

public class Task_2 {
    public static void main(String[] args) {
        // ****************************Task2****************************************************
        // Задано масив цілих чисел. Вивести масив в оберненому порядку, а потім видалити з нього повторні входження кожного елемента.
        int [] mass2={1,1,2,2,3,3,4,4,5,5};
        System.out.print("Input Array: ");
        for (int i=0;i<mass2.length;i++){
            System.out.print(mass2[i] + " ");
        }
        System.out.println();
        System.out.print("Reverse Array: ");
        for (int i=mass2.length-1;i>=0;i--){
            System.out.print(mass2[i] + " ");
        }
        System.out.println();
        int len = mass2.length;
        for ( int i = 0, k = 0; i != len; i++, len = k )
        {
            for ( int j = k = i + 1; j != len; j++ )
            {
                if ( mass2[j] != mass2[i] )
                {
                    if ( k != j ) mass2[k] = mass2[j];
                    k++;
                }
            }
        }
        if ( len != mass2.length )
        {
            int[] b = new int[len];
            for ( int i = 0; i < len; i++ ) b[i] = mass2[i];

            mass2 = b;
        }
        System.out.print("Unique Array: ");
            for (int i=mass2.length-1;i>=0;i--){
            System.out.print(mass2[i] + " ");
        }
          }
}
